#include <iostream>

//using namespace std;

int main()
{
    //To jest zmienna typu prawda/fałsz
    //każda wartość 0 lub poniżej zera oznacza fałsz (false)
    //każda pozostała - prawdę (true)
    //zmienna zajmuje w pamięci 1 bajt
    bool b = false;
    /*
      Zmienna przechowuje wartości POJEDYNCZEGO ZNAKU
      Znaki muszą być z granicy ASCII/Extended ASCII
      (maksymalnie jeden bajt, 8 bit)
      Zmienna zajmuje w pamięci 1 bajt
    */
    char c = 'c';
    /* Pierwszy typ zmiennej liczbowej całkowitej.
     * Pozwala na przechowanie wartości do 16 bitów (2 bajtów)
     * */
    short s = 45;
    /*
     * Podstawowy typ całkowity w C++. Może posiadać
     * różną wielkość (zajętość w pamięci RAM) ponieważ
     * zależy on od platformy systemowej, sprzętowej oraz
     * od kompilatora; przeważnie przyjmuje się, że
     * posiada długość 32 bitów (4 bajty)
     * */
    int i = -157;
    /*
     * Liczba zmiennoprzecinkowa pojedynczej precyzji;
     * Może być zależna od platformy lecz najczęściej
     * jest to 32 bit (4 bajty)
     * */
    float f = 13.7f;
    /*
     * Liczba zmiennoprzecinkowa podwójnej precyzji
     * Precyzja nie mniejsza niż we float, przeważnie
     * posiada 64 bity (8 bajtów)
     * */
    double d = 13.7;
    /*
     * Typ tzw. pusty. Sam w sobie nie może istnieć,
     * są jednak sposoby by go wykorzystać. W poniższej
     * "deklaracji" nie zajmuje dosłownie nawet jednego
     * bitu.
     * */
    //void v = 0;

    int test = 36980078;
    int w = 10;
    unsigned long long equal = test*w;
    unsigned char uc = 245;
    std::cout << static_cast<int>(uc) << ' ' << equal << std::endl;
    return 0;
}
