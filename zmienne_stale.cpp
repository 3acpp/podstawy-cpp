#include <iostream>

//stała w stylu C
//nie może być deklarowana w funkcji (nie powinna)
#define TAB "\t\t\t\t"

int main()
{
    //stała w stylu C++; może być deklarowana w funkcjach
    //chociaż wygląda jak zmienna to jej wartość nie może
    //zostać zmieniona podczas działania programu
    const char* tab = "\t\t";
    //pojedynczy znak; zajmuje ZAWSZE 8 bit (1 bajt)
    //tak naprawdę zmienna przechowuje liczbę całkowitą
    //która interpretowana jest jako znak
    char znak = 'a';
    //zmienna logiczna (prawda/fałsz); przyjmuje fałśz gdy
    //jej wartość wynosi liczbowo 0 (bądź słowo false)
    //w każdym innym wypadku (dowolna wartość liczbowa lub true)
    //przujmuje wartość prawdy
    //zajmuje 8 bit (1 bajt)
    bool logika = true;
    //liczba całkowita krótka (mniejszy zakres)
    //przyjmuje zawsz 16 bit (2 bajty)
    short calkowitaKrotka = -15;
    //liczba całkowita; nie posiada jednoznacznego rozmiaru w
    //pamięci; przeważnie jej długość (bitowa) zależna jest od
    //możliwości platformy (sprzętowej i sytemu operacyjnego)
    //i NIGDY NIE WYNOSI mniej niż short
    //długość int na danej platformie określa się mianem word
    //(słowa) danej platformy
    int liczba = 15;
    //liczba całkowita długa; generalnie powinna być dwukrotnie
    //dłuższa bitowo niż int, jednak dokumnentacja mówi iż jest
    //CO NAJMNIEJ długości int
    long calkowitaDluga = 1678;
    //liczba całkowita poczwórnej długości bitowej;
    //CO NAJMNIEJ długości long
    long long bardzoDluga = -17896;
    //zmienna zmiennoprzecinkowa; można zapisywać liczby
    //ułamkowe (dodatnie i ujemne). Należy pamiętać by przy
    //zapisywanej wartości dodać literę f (oznaczającą float)
    //gdyż kompilator może w przeciwnym wypadku zmienić jej wartość
    //na np. całkowitą bądź obciąć część po przecinku.
    //CO NAJMNIEJ długości int
    float zmiennoprzecinkowa = -14.6f;
    //zmienna zmiennoprzecinkowa podwójnej precyzji;
    //oznacza to, że zawiera dwa razy więcej bitów na zapis części
    //ułamkowej liczby niż float
    //bitowo NIE MNIEJSZA niż float
    double zmiennoprzecinkowaPodwojna = 56.67;

    //zmienna BEZZNAKOWA czyli taka, która przyjmuje TYLKO
    //WARTOŚCI dodatnie; jak poniżej, można wpisać wartość ujemną
    //jednak zostanie ona zmieniona na dodatnią (konwersja
    //na poziomie bitów)
    //Bitowo (kompilacja na platformie ze słowem 32 bit)
    //zmienna (wartość -15) wygląda następująco:
    //MSB -> 11111111111111111111111111110001 <- LSB
    //MSB - Most Significant Bit (najbardziej znaczący bit)
    //najbardziej znaczący gdyż jego zmiana (z 0 na 1 i/lub na
    //odwrót) zmienia wartość zmiennej O POŁOWĘ
    //LSB - Less Significant Bit (najmniej znaczący bit)
    //najmniej znaczącu gdyż jego zmiana może zmienić zapisaną
    //wartość jedynie o 1

    //Ponieważ najstarszy bit nie jest już ujemny MSB
    //przyjmuje wartość dodatnią i w tym momencie zakres z
    //-‭2147483648  do ‭2147483647‬ (zawiera 0 - naturalne, parzyste)
    //zmienia się na zakres
    //0 do ‭4294967295‬ (zakres się podwaja)
    //co oznacza, że od maksymalnej naturalnej wartości odejmujemy
    //14 (trzy bity z wartością 0 dają łącznie wartość 14 dziesiętnie)
    //stąd wynik 4294967281 (‭4294967295‬ - 14)
    unsigned int naturalna = -15;
    std::cout << "Program wyswietla typy podstawowych zmiennych,"<<
                 " wartości zapisane pod zmiennymi oraz " <<
                 "ile zajmuja one w pamieci RAM (w bajtach)\n\n" <<
                 "TYP DANYCH"<<tab<<"ZAPISANA WARTOSC"<<tab<<"DLUGOSC W RAM\n" <<
                 "---------------------------------------------------------------------\n" <<
                 "bool" << TAB << logika << TAB << sizeof(logika) <<
                 "\nchar" << TAB << znak << TAB << sizeof(znak) <<
                 "\nshort" << TAB << calkowitaKrotka << TAB << sizeof(calkowitaKrotka) <<
                 "\nint" << TAB << liczba << TAB << sizeof(liczba) <<
                 "\nlong" << TAB << calkowitaDluga << TAB << sizeof(calkowitaDluga) <<
                 "\nlong long" << TAB << bardzoDluga << TAB << sizeof(bardzoDluga) <<
                 "\nfloat" << TAB << zmiennoprzecinkowa << TAB << sizeof(zmiennoprzecinkowa) <<
                 "\ndouble" << TAB << zmiennoprzecinkowaPodwojna << TAB << sizeof(zmiennoprzecinkowaPodwojna) <<
                 "\nunsigned" << TAB << naturalna << TAB << sizeof(naturalna) <<
                 "\n";
    //ZNAK TEŻ JEST LICZBĄ!! (zamiana na znaki
    //drukarskie/nie drukarskie następuje poprzez kodowanie,
    //takie jak np. ASCII, UTF-8, ISO-8859-2 itp.)
    //UWAGA! char domyślnie obsuguje jedynie ASCII (bądź
    //extended ASCII)
    //poniżej przekroczenie zakresu char ze znakiem
    //(przemieni wartość liczbową z 220 na -36 -> OPERACJE BITOWE)
    //jednak C++ poprawnie rozpozna znak z tzw. Extended ASCII
    char znak2 = 220;
    std::cout << znak2 << std::endl;
    //zmienna znakowa zapisana bez znaku; jej wartość się nie zmieni
    //i też wypisze znak z Extended ASCII
    unsigned char znak3 = 220;
    std::cout << znak3 << std::endl;

    //sprawdzenie wartości liczowy z znakach (konwersje
    //będą omówione później)
    std::cout << static_cast<int>(znak2) << ' ' <<
                 static_cast<int>(znak3) << std::endl;
    return 0;
}
